package com.gjj.igden.service.test;

import com.gjj.igden.model.Account;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.watchlist.WatchListDescServiceService;
import com.gjj.igden.service.test.daostub.WatchListDescDaoStub;
import com.gjj.igden.model.IWatchListDesc;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@Configuration
@ComponentScan(basePackageClasses = {WatchListDescServiceService.class,
  WatchListDescDaoStub.class})
class SpringTextContext {
}

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringTextContext.class)
public class WatchListDescServiceTest {
  @Autowired
  private WatchListDescServiceService watchListDescService;

  @Test
  public void simpleReadTest() throws Exception {
    watchListDescService.getStockSymbolsList(1).forEach(System.out::println);
  }

  @Test
  public void testCreateH2DataBaseTest() {
    List<WatchListDesc> dataSetList = watchListDescService.getDataSetsAttachedToAcc(1);
    final int expectedDataSetsAmount = 4;
    Assert.assertEquals(expectedDataSetsAmount, dataSetList.size());
  }

  @Test
  public void testReturnBarList() {
    IWatchListDesc dataSet = watchListDescService.getDataSetsAttachedToAcc(2).get(0);
    System.out.println(dataSet.getWatchListName());
    Assert.assertNotNull(dataSet);
    Assert.assertEquals("test-aapl-5minBar-preMarketdata", dataSet.getWatchListName());
  }

  @Test
  public void testDelete02() throws Exception {
    List<WatchListDesc> dataSetList = watchListDescService.getDataSetsAttachedToAcc(1);
    final int expectedDataSetsAmount = dataSetList.size();
    dataSetList.forEach(p -> System.out.println(p.getDatasetId()));
    boolean deleteResultFlag = watchListDescService.delete(dataSetList.get(0));
    Assert.assertTrue(deleteResultFlag);
    System.out.println("after deletion ");
    dataSetList = watchListDescService.getDataSetsAttachedToAcc(1);
    dataSetList.forEach(p -> System.out.println(p.getDatasetId()));
    Assert.assertNotEquals(expectedDataSetsAmount, dataSetList.size());
  }

  @Test
  public void testCreateDataSet() throws Exception {
    int accId = 1;
    Account account = new Account();
    account.setId(accId);
    WatchListDesc newWatchList = watchListDescService.getWatchListDesc(1, accId);
    List<WatchListDesc> dataSetList = watchListDescService.getDataSetsAttachedToAcc(1);
    dataSetList.forEach(p -> System.out.print(p.getDatasetId() + " ; "));
    int expectedDataSetsAmountAfterDeletion = 4;
    Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());
    Assert.assertNotNull(newWatchList);
    newWatchList.setDatasetId(111);
    newWatchList.setAccountList(Lists.newArrayList(account));
    newWatchList.setWatchListName("just testing around");
    Assert.assertTrue(watchListDescService.create(newWatchList, accId));
    dataSetList = watchListDescService.getDataSetsAttachedToAcc(1);
    dataSetList.forEach(p -> System.out.print(p.getDatasetId() + " ; "));
    expectedDataSetsAmountAfterDeletion = 5;
    Assert.assertEquals(expectedDataSetsAmountAfterDeletion, dataSetList.size());
  }

  @Test
  public void testUpdate() throws Exception {
    final int accId = 1;
    WatchListDesc dataSet = watchListDescService.getWatchListDesc(1, accId);
    dataSet.setWatchListName("test update");
    dataSet.setDatasetId(1);
    watchListDescService.update(dataSet);
    final String dataSetNameDirect = watchListDescService.getWatchListDesc(1, 1).getWatchListName();
    Assert.assertEquals("test update", dataSetNameDirect);
  }

  @Test
  public void test01Read() throws Exception {
    List<WatchListDesc> watchListDescs = watchListDescService.getDataSetsAttachedToAcc(1);
    final int size = 4;
    Assert.assertEquals(size,
      watchListDescs.size());
  }

  /*










  */
}
