package com.gjj.igden.service.accountService;

import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.dao.WatchListDescDao;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.util.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

@Service
public class AccountService {
  @Autowired
  private AccountDao accountDao;
  @Autowired
  private WatchListDescDao watchListDescDao;

  public List<Account> getAccountList() {
    return accountDao.getAllAccounts();
  }

  @Transactional
  public boolean createAccount(Account account) {
    boolean resultFlag = accountDao.create(account);
    if (resultFlag) {
      return true;
    } else {
      System.err.println(" something bad happen - account wasn't added ");
      return false;
    }
  }

  @Transactional
  public boolean updateAccount(Account account) {
    Account oldAccount = accountDao.get(account.getId());
    oldAccount.setAccountName(account.getAccountName());
    oldAccount.setEMail(account.geteMail());
    oldAccount.setAdditionalInfo(account.getAdditionalInfo());
      System.err.println(oldAccount.toString());
    return accountDao.update(oldAccount);
  }

  public Account retrieveAccount(int accId) {
    Account user = accountDao.get(accId);
    List<WatchListDesc> dataSetList = watchListDescDao.getDataSetsAttachedToAcc(accId);
    user.setDataSets(dataSetList);
    return user;
  }

  public boolean delete(int id) {
    return accountDao.delete(id);
  }

  public boolean setImage(int accId, InputStream is) {
    return accountDao.setImage(accId, is);
  }

  public byte[] getImage(int accId){
    return accountDao.getImage(accId);
  }

  public String parseToXML(Account account) throws ServiceException {
    StringWriter sw = new StringWriter();
    try {
      JAXBContext context = JAXBContext.newInstance(Account.class);
      Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      marshaller.marshal(account, sw);
    } catch (JAXBException e) {
      throw new ServiceException(e.getMessage());
    }
    return sw.toString();
  }

  public Account parseFromXML(File file) throws ServiceException {
    Account account;
    try {
      JAXBContext jc = JAXBContext.newInstance(Account.class);
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      account = (Account) unmarshaller.unmarshal(file);
    } catch (JAXBException e) {
      throw new ServiceException(e.getMessage());
    }
    return account;
  }
}
