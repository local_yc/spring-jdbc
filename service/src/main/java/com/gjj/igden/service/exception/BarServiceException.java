package com.gjj.igden.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BarServiceException extends RuntimeException{
  private static final String DEFAULT_MESSAGE = "BAD_REQUEST";

  public BarServiceException() {
    super(DEFAULT_MESSAGE);
  }

  public BarServiceException(String message) {
    super(message);
  }
}
