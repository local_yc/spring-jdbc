package com.gjj.igden.service.barService;

import com.gjj.igden.dao.BarDao;
import com.gjj.igden.dao.daoUtil.DaoException;
import com.gjj.igden.model.Bar;
import com.gjj.igden.service.exception.BarServiceException;
import com.gjj.igden.service.util.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BarService {
  @Autowired
  private BarDao barDao;

  public List<Bar> getBarList(String instId) {
    return barDao.getBarList(instId);
  }

  public Bar getSingleBar(long barId, String instId) {
    return barDao.getSingleBar(barId, instId);
  }

  public boolean update(Bar bar) {
    return barDao.updateBar(bar);
  }

  public boolean createBar(Bar bar) throws ServiceException {
    try {
      return barDao.createBar(bar);
    } catch (DaoException e) {
      throw new ServiceException.ExceptionBuilder().setException(e).build();
    }
  }

  public boolean deleteBar(Bar bar) {
    return barDao.deleteBar(bar);
  }

  public List<String> searchTickersByChars(String tickerNamePart) {
    return barDao.searchTickersByChars(tickerNamePart);
  }

  /**
   * Returns list of {@linkplain Bar} by market and ticker list
   * @param query string in the following format 'market:list of tickers separated by coma'
   * @return list of {@linkplain Bar}
   */
  public List<Bar> listByMarketAndTickerList(String query) {
    String[] split = query.split(":");
    if (split.length != 2) {
      throw new BarServiceException();
    }
    String market = split[0];
    String[] tickerList = split[1].split(",");
    if (tickerList.length < 1) {
      throw new BarServiceException();
    }
    return barDao.listByMarketAndTickerList(market, tickerList);
  }
}