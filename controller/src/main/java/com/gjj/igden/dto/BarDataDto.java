package com.gjj.igden.dto;

public class BarDataDto {
  private double l_cur;
  private String t;

  public double getL_cur() {
    return l_cur;
  }

  public void setL_cur(double l_cur) {
    this.l_cur = l_cur;
  }

  public String getT() {
    return t;
  }

  public void setT(String t) {
    this.t = t;
  }
}
