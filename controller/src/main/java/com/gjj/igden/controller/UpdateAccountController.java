package com.gjj.igden.controller;

import com.gjj.igden.model.Account;
import com.gjj.igden.service.accountService.AccountService;
import com.gjj.igden.service.util.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.beans.PropertyEditorSupport;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
public class UpdateAccountController {
	@Autowired
	private AccountService accountService;

	@InitBinder
	public void customizeBinding(WebDataBinder binder) {
		binder.registerCustomEditor(LocalDate.class, "birthday", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				setValue(LocalDate.parse(text, DateTimeFormatter.ISO_DATE));
			}
		});
	}

	@GetMapping("updateAccountController")
	protected String doGet(Model model) {
		model.addAttribute("accountChanged", new Account());
		return "updateAccount";
	}

	@GetMapping("exportToXML")
	@ResponseBody
	public byte[] exportToXML(@SessionAttribute Account account, HttpServletResponse response) throws ServletException {
		response.setContentType("application/xml");
		response.addHeader("Content-Disposition", "attachment; filename=account.xml");
		try {
			return accountService.parseToXML(account).getBytes();
		} catch (ServiceException e) {
			throw new ServletException(e.getMessage(), e);
		}
	}

	@PostMapping("uploadAccountXML")
	protected String uploadAccountXML(@RequestParam MultipartFile file,
									  @SessionAttribute Account account) throws ServletException {
		try {
			account = accountService.parseFromXML(multipartToFile(file));
		} catch (ServiceException e) {
			throw new ServletException(e.getMessage(), e);
		}
		return "redirect: updateAccountController";
	}

	private File multipartToFile(MultipartFile multipart) throws ServletException {
		File convFile = new File(multipart.getOriginalFilename());
		try {
			multipart.transferTo(convFile);
		} catch (IOException e) {
			throw new ServletException(e.getMessage(), e);
		}
		return convFile;
	}
}

