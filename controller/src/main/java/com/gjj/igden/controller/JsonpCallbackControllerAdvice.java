package com.gjj.igden.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

@ControllerAdvice
public class JsonpCallbackControllerAdvice extends AbstractJsonpResponseBodyAdvice {
  public JsonpCallbackControllerAdvice() {
    super("callback");
  }
}
