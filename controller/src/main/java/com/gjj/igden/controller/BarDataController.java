package com.gjj.igden.controller;

import com.gjj.igden.dto.BarDataDto;
import com.gjj.igden.model.Bar;
import com.gjj.igden.service.barService.BarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class BarDataController {
  @Autowired
  private BarService service;

  @GetMapping("/view-data")
  public String viewAccount(ModelMap model, @RequestParam String stockSymbol) {
    List<Bar> barList = service.getBarList(stockSymbol);
    model.addAttribute("barData", barList);
    return "view-data";
  }

  @GetMapping("/search")
  public String searchGet1() {
    return "search";
  }

  @GetMapping("/DataController")
  public String searchGet2(ModelMap model, @RequestParam String searchParam) {
    List<String> tickets = service.searchTickersByChars(searchParam);
    model.addAttribute("THE_SEARCH_RESULT_LIST", tickets);
    return "search";
  }

  @PostMapping("/search")
  public String searchPost(ModelMap model, @RequestParam String stockSymbol) {
    List<Bar> barList = service.getBarList(stockSymbol);
    model.addAttribute("barData", barList);
    return "view-data";
  }

  @ResponseBody
  @GetMapping(value = "/ticker-data")
  public List<BarDataDto> listMarketData(String client, @RequestParam("q") String query) {
        List<Bar> barList = service.listByMarketAndTickerList(query);

    return barList.stream()
            .map(this::barToBarDataDto)
            .collect(Collectors.toList());
  }

  private BarDataDto barToBarDataDto(Bar bar) {
    BarDataDto barDataDto = new BarDataDto();
    barDataDto.setL_cur(bar.getLow());
    barDataDto.setT(bar.getInstId().getSymbol());
    return barDataDto;
  }
}
