package com.gjj.igden.controller;

import com.gjj.igden.model.Users;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TestController {
  @GetMapping("/lazyRowLoad")
  public ModelAndView printWelcome(@ModelAttribute("user") Users user) {
    ModelAndView mav = new ModelAndView("lazyRowLoad");
    mav.addObject("message", "Hello World!!!");
    return mav;
  }
}
