package com.gjj.igden.controller;

import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.service.watchlist.WatchListDescServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WatchListDescController {
  @Autowired
  private WatchListDescServiceService service;

  @GetMapping("/view-watchlist")
  public String viewAccount(ModelMap model, @RequestParam int id) {
    model.addAttribute("stockSymbolsList", service.getStockSymbolsList(id));
    model.addAttribute("watchListId", id);
    return "view-watchlist";
  }

  @PostMapping(value = "/lazyRowAdd.web")
  public String lazyRowAdd(@ModelAttribute("theWatchListDesc") WatchListDesc theWatchListDesc,
                           @ModelAttribute("username1") String watchlistName, @RequestParam("id") int accId) {
    System.out.println(accId);
    service.create(theWatchListDesc, accId);
    return "redirect:/view-account?id=2";
  }
}
