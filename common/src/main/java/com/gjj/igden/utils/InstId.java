package com.gjj.igden.utils;

import com.google.common.base.Objects;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Instrument identifier
 */
@Embeddable
public class InstId implements Serializable {
  private final static String SEPARATOR = "@";
  @Column(name = "instId_fk")
  private String symbol;
  /**
   * mdId - i suppose this only for bar data_containers because for tick data_containers it is will
   * be impossible to supply all needed id
   */
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "md_id")
  protected Long mdId;
  @Transient
  private String exchId;
  @Transient
  private Exchange exchange;

  public InstId() {
  }

  public InstId(String symbol, Long mdId) {
    this(symbol);
    this.symbol = symbol;
    this.mdId = mdId;
  }

  public InstId(String str) {
    validateCorrectString(str);
    String[] tokens = str.split("@");
    symbol = tokens[0].toUpperCase();
    exchId = tokens[1].toUpperCase();
  }

  public InstId(String symbol, String exchId) {
    this.symbol = symbol;
    this.exchId = exchId;
  }

  public Exchange getExchange() {
    if (java.util.Objects.equals(exchId, "NASDAQ")) {
      return Exchange.NASDAQ;
    }
    if (java.util.Objects.equals(exchId, "NYSE")) {
      return Exchange.NYSE;
    }
    return exchange;
  }

  private void validateCorrectString(String str) {
  }

  public String getExchId() {
    return exchId;
  }

  public String getSymbol() {
    return symbol;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof InstId)) {
      return false;
    }
    InstId altInstId = (InstId) o;
    return Objects.equal(symbol, altInstId.symbol) &&
            Objects.equal(exchId, altInstId.exchId) &&
            Objects.equal(mdId, altInstId.getMdId());
  }

  public Exchange getExchange(String exchId) {
    if (exchId.equalsIgnoreCase("NYSE")) {
      return Exchange.NYSE;
    } else if (exchId.equalsIgnoreCase("nasdaq")) {
      return Exchange.NASDAQ;
    } else {
      throw new IllegalArgumentException("your exchange id is not correct");
    }
  }

  public Long getMdId() {
    return mdId;
  }

  public void setMdId(Long mdId) {
    this.mdId = mdId;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(symbol, exchId, mdId);
  }

  @Override
  public String toString() {
    return symbol + SEPARATOR + exchId;
  }
}

