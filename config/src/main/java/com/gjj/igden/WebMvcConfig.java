package com.gjj.igden;

import com.gjj.igden.controller.AccountController;
import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.service.accountService.AccountService;
import com.gjj.igden.service.barService.BarService;
import com.gjj.igden.service.watchlist.WatchListDescServiceService;
import liquibase.integration.spring.SpringLiquibase;
import org.apache.log4j.BasicConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * inspired by : https://www.youtube.com/watch?v=5BY9YxdMg8I
 */
@EnableWebMvc
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackageClasses = {AccountController.class,
        AccountDao.class, WatchListDescServiceService.class,
        WatchListDescServiceService.class, AccountService.class, BarService.class})
@Import({SecurityConfig.class})
public class WebMvcConfig extends WebMvcConfigurerAdapter {

  @PostConstruct
  public void init() {
    BasicConfigurator.configure();
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/webjars/**").addResourceLocations("/webjars/");
  }

  @Bean
  public MultipartResolver multipartResolver() {
    return new StandardServletMultipartResolver();
  }

  @Bean
  public DataSource dataSource() {
    final JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
    dataSourceLookup.setResourceRef(true);
    return dataSourceLookup.getDataSource("jdbc/ya_web_app_db");
  }

  @Bean
  @Autowired
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
    LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
    emf.setDataSource(dataSource);
    emf.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
    emf.setPackagesToScan("com.gjj.igden.model");
    emf.setJpaProperties(new Properties() {
      {
        setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
      }
    });
    return emf;
  }

  @Bean
  @Autowired
  public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
    JpaTransactionManager txManager = new JpaTransactionManager();
    txManager.setEntityManagerFactory(emf);
    return txManager;
  }

    @Bean
    public SpringLiquibase liquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setChangeLog("classpath:liquibase/db-changelog-master.xml");
        liquibase.setDataSource(dataSource());
        return liquibase;
    }

  @Bean
  public UrlBasedViewResolver urlBasedViewResolver() {
    UrlBasedViewResolver resolver = new UrlBasedViewResolver();
    resolver.setPrefix("/WEB-INF/");
    resolver.setSuffix(".jsp");
    resolver.setViewClass(JstlView.class);
    return resolver;
  }
}