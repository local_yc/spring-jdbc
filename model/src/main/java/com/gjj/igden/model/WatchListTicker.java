package com.gjj.igden.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "wl_tickers")
public class WatchListTicker implements Serializable{
  @Id
  @Column(name = "instId")
  private String instId;

  @Id
  @Column(name = "watchlist_id_fk")
  private Integer watchListId;

  @ManyToOne
  @MapsId("watchListId")
  @JoinColumn(name = "watchlist_id_fk", referencedColumnName = "data_set_id", insertable = false, updatable = false)
  private WatchListDesc watchListDesc;

  public Integer getWatchListId() {
    return watchListId;
  }

  public void setWatchListId(Integer watchListId) {
    this.watchListId = watchListId;
  }

  public String getInstId() {
    return instId;
  }

  public void setInstId(String instId) {
    this.instId = instId;
  }

}
