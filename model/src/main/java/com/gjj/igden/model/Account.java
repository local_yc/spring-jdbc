package com.gjj.igden.model;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "account")
@XmlRootElement(name = "account")
@XmlAccessorType(XmlAccessType.FIELD)
public class Account {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "account_id")
  @XmlAttribute
  private Integer id;
  @Column(name = "account_name")
  private String accountName;
  @Column(name = "email")
  private String eMail;
  @Column(name = "additional_info")
  private String additionalInfo;
  @Column(name = "password")
  private String password;
  @ManyToMany
  @JoinTable(name = "account_data_set",
          joinColumns = @JoinColumn(name = "account_id"),
          inverseJoinColumns = @JoinColumn(name = "data_set_id")
  )
  @XmlTransient
  private List<WatchListDesc> descriptions;
  @Column(name = "creation_date")
  private String creationDate;
  @Column(name = "image")
  @XmlTransient
  private byte[] image;

  public Account() {
  }

  public Account(String accountName, String eMail, String additionalInfo, String password,
                 List<WatchListDesc> descriptions, String creationDate) {
    this.accountName = accountName;
    this.eMail = eMail;
    this.additionalInfo = additionalInfo;
    this.password = password;
    this.descriptions = descriptions;
    this.creationDate = creationDate;
  }

  public Account(int id, String accountName, String eMail,
                 String additionalInfo, String password,
                 List<WatchListDesc> dataSets, String creationDate) {
    this.id = id;
    this.accountName = accountName;
    this.eMail = eMail;
    this.additionalInfo = additionalInfo;
    this.password = password;
    this.descriptions = dataSets;
    this.creationDate = creationDate;
  }

  public Account(String accountName, String eMail,
                 String additionalInfo) {
    this.accountName = accountName;
    this.eMail = eMail;
    this.additionalInfo = additionalInfo;
  }

  public Account(Integer id, String accountName, String eMail, String additionalInfo,
                 String creationDate) {
    this.id = id;
    this.accountName = accountName;
    this.eMail = eMail;
    this.additionalInfo = additionalInfo;
    this.creationDate = creationDate;
  }

/*  public Account(String accountName, String eMail, String additionalInfo,
                 List<WatchListDesc> dataSets) {
    this(null, accountName, eMail, additionalInfo, dataSets);
  }*/

  public byte[] getImage() {
      return image;
  }
  public void setImage(byte[] image) {
      this.image = image;
  }

    public Account(int accountId, String accountName, String eMail, String additionalInfo) {
    this(accountId, accountName, eMail, additionalInfo, (String) null);
  }

  public String geteMail() {
    return eMail;
  }

  public void seteMail(String eMail) {
    this.eMail = eMail;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<WatchListDesc> getAttachedWatchedLists() {
    return descriptions;
  }

  public void setDescriptions(List<WatchListDesc> descriptions) {
    this.descriptions = descriptions;
  }

  public String getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(String creationDate) {
    this.creationDate = creationDate;
  }

  public String getEMail() {
    return eMail;
  }

  public void setEMail(String eMail) {
    this.eMail = eMail;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public List<WatchListDesc> getDataSets() {
    return descriptions;
  }

  public void setDataSets(List<WatchListDesc> dataSets) {
    this.descriptions = dataSets;
  }

  public Integer getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    return com.google.common.base.Objects.hashCode(eMail, accountName);
  }

  @Override
  public boolean equals(Object obj) {
    return obj instanceof Account && ((Account) obj).eMail.equals(this.eMail) &&
            Objects.equals(((Account) obj).accountName, this.accountName);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(" [id=");
    builder.append(id);
    builder.append(", accountName= ");
    builder.append(accountName);
    builder.append(", eMail= ");
    builder.append(eMail);
    builder.append(", additionalInfo= ");
    builder.append(additionalInfo);
    builder.append(", password= ");
    builder.append(password);
    builder.append(", data_containers Sets names= ");
    builder.append(descriptions);
    builder.append("]\n");
    return builder.toString();
  }
}
