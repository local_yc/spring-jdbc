package com.gjj.igden.model;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class WatchListPrimaryKey implements Serializable{
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "data_set_id")
  private Integer watchListId;
  @Column(name = "account_fk_id")
  private int accountId;

  public WatchListPrimaryKey() {
  }

  public WatchListPrimaryKey(Integer watchListId, int accountId) {
    this.watchListId = watchListId;
    this.accountId = accountId;
  }

  public Integer getWatchListId() {
    return watchListId;
  }

  public void setWatchListId(Integer watchListId) {
    this.watchListId = watchListId;
  }

  public int getAccountId() {
    return accountId;
  }

  public void setAccountId(int accountId) {
    this.accountId = accountId;
  }
}
