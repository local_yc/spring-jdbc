package com.gjj.igden.model;

import com.gjj.igden.utils.InstId;
import com.gjj.igden.utils.InterfaceOHLCData;
import com.google.common.base.Objects;
import org.javatuples.Ennead;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Set;

@SuppressWarnings("WeakerAccess")
@Entity
@Table(name = "market_data")
public class Bar extends MarketData implements InterfaceOHLCData {
  @Transient
  protected int dataSetId;
  @Transient
  protected int barSize;
  @Column(name = "open")
  protected double open;
  @Column(name = "high")
  protected double high;
  @Column(name = "low")
  protected double low;
  @Column(name = "close")
  protected double close;
  @Column(name = "vol")
  protected long volume;
  @Column(name = "additional_info")
  protected String logInfo;
  @Column(name = "ticker")
  private String ticker;

  public void setLogInfo(String logInfo) {
    this.logInfo = logInfo;
  }

  public Bar() {
  }

  public Bar(long mdId, String instId) {
    this.instId = new InstId(instId, mdId);
  }

  public Bar(Bar bar) {
    super(bar.instId, bar.dateTime);
    this.dataSetId = bar.dataSetId;
    this.barSize = bar.barSize;
    this.open = bar.open;
    this.high = bar.high;
    this.low = bar.low;
    this.close = bar.close;
    this.volume = bar.volume;
    this.logInfo = bar.logInfo;
  }

  public Bar(
          InstId instId,
          int barSize,
          long dateTime,
          double open,
          double high,
          double low,
          double close,
          long volume,
          String logInfo) {
    super(instId, dateTime);
    this.barSize = barSize;
    this.open = open;
    this.high = high;
    this.low = low;
    this.close = close;
    this.volume = volume;
    this.logInfo = logInfo;
  }


  public String getTicker() {
    return ticker;
  }

  public void setTicker(String ticker) {
    this.ticker = ticker;
  }

  public Long getMdId() {
    InstId instId = getInstId();
    return instId != null ? instId.getMdId() : null;
  }

  public void setMdId(Long mdId) {
    InstId instId = getInstId();
    if (instId != null) {
      instId.setMdId(mdId);
    } else {
      throw new IllegalStateException();
    }
  }

  public int getDataSetId() {
    return dataSetId;
  }

  public void setDataSetId(int dataSetId) {
    this.dataSetId = dataSetId;
  }

  public int getBarSize() {
    return barSize;
  }

  public void setBarSize(int barSize) {
    this.barSize = barSize;
  }

  public double getOpen() {
    return open;
  }

  public void setOpen(double open) {
    this.open = open;
  }

  public double getHigh() {
    return high;
  }

  public void setHigh(double high) {
    this.high = high;
  }

  public double getLow() {
    return low;
  }

  public void setLow(double low) {
    this.low = low;
  }

  public double getClose() {
    return close;
  }

  public void setClose(double close) {
    this.close = close;
  }

  public long getVolume() {
    return volume;
  }

  public void setVolume(long volume) {
    this.volume = volume;
  }

  public String getLogInfo() {
    return logInfo;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Bar bar = (Bar) o;
    return Objects.equal(instId, bar.instId) &&
            Objects.equal(dateTime, bar.dateTime) &&
            // TODO m high priority: the line below - in some moment barSize is not setted up
            // TODO m high priority: use test createDataSetTestImportant() to determine the
            // TODO m high priority: bug source
            //	Objects.equal(barSize, bar.barSize) &&
            Objects.equal(high, bar.high) &&
            Objects.equal(low, bar.low) &&
            Objects.equal(open, bar.open) &&
            Objects.equal(close, bar.close) &&
            Objects.equal(volume, bar.volume);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(instId, dateTime, barSize, high, low, open, close, volume);
  }

  @Override
  public void reset() {
    super.reset();
    barSize = -1;
    high = 0.0;
    low = 0.0;
    open = 0.0;
    close = 0.0;
    volume = 0;
    logInfo = null;
  }

  public void copy(Bar bar) {
    this.instId = bar.instId;
    this.dateTime = bar.dateTime;
    this.barSize = bar.barSize;
    this.high = bar.high;
    this.low = bar.low;
    this.open = bar.open;
    this.close = bar.close;
    this.volume = bar.volume;
    this.logInfo = bar.logInfo;
  }

  @Override
  public Ennead<InstId, Long, Long, Integer, Double, Double, Double, Double, Long> getMainData() {
    return null;
  }

  public void setMainData(Ennead<InstId, Long, Long, Integer, Double, Double,
          Double, Double, Long> ennead) {
    this.instId = ennead.getValue0();
    this.dateTime = ennead.getValue2();
    this.barSize = ennead.getValue3();
    this.high = ennead.getValue4();
    this.low = ennead.getValue5();
    this.open = ennead.getValue6();
    this.close = ennead.getValue7();
    this.volume = ennead.getValue8();
    // this.logInfo = ennead.logInfo;
  }
}
