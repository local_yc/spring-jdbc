package com.gjj.igden.model;

import org.apache.commons.collections4.FactoryUtils;
import org.apache.commons.collections4.list.LazyList;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "data_set")
public class WatchListDesc implements IWatchListDesc {
  @Id
  @Column(name = "data_set_id")
  private Integer datasetId;
  @Column(name = "data_set_name")
  private String watchListName;
  @Column(name = "data_set_description")
  private String watchListDetails;
  @OneToMany(mappedBy = "watchListDesc", fetch = FetchType.EAGER)
  private List<WatchListTicker> watchListTickerList;
  @Column(name = "market_data_frequency")
  private int marketDataFrequency;
  @Column(name = "data_providers")
  private String dataProviders;
  @ManyToMany
  @JoinTable(name = "account_data_set",
          joinColumns = @JoinColumn(name = "data_set_id"),
          inverseJoinColumns = @JoinColumn(name = "account_id")
  )
  private List<Account> accountList;
  @Transient
  private List<String> stockSymbolsList;
  @Transient
  private List operationParameterses = LazyList.lazyList(new ArrayList<>(),
          FactoryUtils.instantiateFactory(OperationParameters.class));

    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }

    public Integer getDatasetId() {
        return datasetId;
    }

    public void setDatasetId(Integer datasetId) {
        this.datasetId = datasetId;
    }

    public List<String> getStockSymbolsList() {
    return stockSymbolsList;
  }

  public void setStockSymbolsList(List<String> stockSymbolsList) {
    this.stockSymbolsList = stockSymbolsList;
  }

  public List<WatchListTicker> getWatchListTickerList() {
    return watchListTickerList;
  }

  public void setWatchListTickerList(List<WatchListTicker> watchListTickerList) {
    this.watchListTickerList = watchListTickerList;
  }

  public void setStockSymbolsListFromOperationList(List<OperationParameters> stockSymbolsList) {
    List<String> stringList = stockSymbolsList
            .stream()
            .map(OperationParameters::getName)
            .collect(Collectors.toList());
    this.stockSymbolsList = stringList;
  }

  public List<OperationParameters> getOperationParameterses() {
    return operationParameterses;
  }

  public void setOperationParameterses(
          List<OperationParameters> operationParameterses) {
    this.operationParameterses = operationParameterses;
  }

  public String getWatchListName() {
    return watchListName;
  }

  public void setWatchListName(String watchListName) {
    this.watchListName = watchListName;
  }

  public String getWatchListDetails() {
    return watchListDetails;
  }

  public void setWatchListDetails(String watchListDetails) {
    this.watchListDetails = watchListDetails;
  }

  public int getMarketDataFrequency() {
    return marketDataFrequency;
  }

  public void setMarketDataFrequency(int marketDataFrequency) {
    this.marketDataFrequency = marketDataFrequency;
  }

  public String getDataProviders() {

    // TODO m it should be List<providerID> like phone in social network
    return dataProviders;
  }

  public void setDataProviders(String dataProviders) {
    this.dataProviders = dataProviders;
  }

  public WatchListDesc() {
  }

  public WatchListDesc(int watchListId, String watchListName,
                       String watchListDetails, int marketDataFrequency, String dataProviders) {
    this.datasetId = watchListId;
    this.watchListName = watchListName;
    this.watchListDetails = watchListDetails;
    this.marketDataFrequency = marketDataFrequency;
    this.dataProviders = dataProviders;
  }

  @Override
  public String toString() {
    return String.valueOf(" data set id = " + this.getDatasetId() + "\n " +
            "market data freq = " + this.getMarketDataFrequency() + "\n " +
            "data set name = " + this.getWatchListName() + "\n " +
            "data set description = " + this.getWatchListDetails() + "\n ");
  }
}
