package com.gjj.igden.model;

import java.io.Serializable;
import java.util.List;

public interface IWatchListDesc extends Serializable {
  List<String> getStockSymbolsList();

  void setStockSymbolsList(List<String> stockSymbolsList);

  void setStockSymbolsListFromOperationList(List<OperationParameters> stockSymbolsList);

  String getWatchListName();

  void setWatchListName(String watchListName);

  String getWatchListDetails();

  void setWatchListDetails(String watchListDetails);

  int getMarketDataFrequency();

  void setMarketDataFrequency(int marketDataFrequency);

  String getDataProviders();

  void setDataProviders(String dataProviders);

  String toString();

  List<OperationParameters> getOperationParameterses();

  void setOperationParameterses(List<OperationParameters> operationParameterses);
}
