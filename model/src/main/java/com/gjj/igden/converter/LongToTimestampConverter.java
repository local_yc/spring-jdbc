package com.gjj.igden.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;

@Converter
public class LongToTimestampConverter implements AttributeConverter<Long, Timestamp> {
  @Override
  public Timestamp convertToDatabaseColumn(Long attribute) {
    return new Timestamp(attribute);
  }

  @Override
  public Long convertToEntityAttribute(Timestamp dbData) {
    return dbData.getTime();
  }
}
