package com.gjj.igden.dao.daoimpl;

import com.gjj.igden.dao.BarDao;
import com.gjj.igden.dao.BaseDao;
import com.gjj.igden.dao.daoUtil.DaoException;
import com.gjj.igden.model.Bar;
import com.gjj.igden.utils.InstId;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("SameParameterValue")
@Repository("barDao")
public class BarDaoImpl extends BaseDao<Integer, Bar> implements BarDao {
  private static final String SELECT_BY_MARKET_AND_TICKERS = "SELECT b FROM Bar b where b.instId.symbol LIKE :market AND b.ticker IN (:tickers)";

  public Bar getSingleBar(long md_id, String instId) {
    return entityManager.find(Bar.class, new InstId(instId, md_id));
  }

  public List<Bar> getBarList(String instId) {
    String sqlQuery = "SELECT b FROM Bar b WHERE b.instId.symbol = :instId ";
    return entityManager.createQuery(sqlQuery, Bar.class)
            .setParameter("instId", instId)
            .getResultList();
  }

  @Transactional
  public boolean createBar(Bar bar) throws DaoException {
    try {
      bar.getDateTimeMySQLFormat();
    } catch (ParseException e) {
      throw new DaoException.ExceptionBuilder().setException(e).build();
    }
    bar = save(bar);
    return bar != null;
  }

  @Transactional
  public boolean updateBar(Bar bar) {
    Bar entity = entityManager.find(Bar.class, new InstId(bar.getInstId().getSymbol(), bar.getMdId()));
    if (entity != null) {
      entity.setDataSetId(bar.getDataSetId());
      entity.setInstId(bar.getInstId().toString());
      entity.setLogInfo(bar.getLogInfo());
      entityManager.merge(entity);
      return true;
    }
    return false;
  }

  @Transactional
  public boolean deleteBar(long mdId, String instId) {
      return deleteById(new InstId(instId, mdId));
  }

  @Transactional
  public boolean deleteBar(Bar bar) {
      InstId instId = bar.getInstId();
      return deleteById(instId);
  }

    private boolean deleteById(InstId instId) {
        Bar entity = entityManager.find(Bar.class, instId);
        if (entity != null) {
            delete(entity);
            return true;
        }
        return false;
    }

    public List<String> searchTickersByChars(String tickerNamePart) {
    String sqlQuery = "SELECT b FROM Bar b WHERE b.instId.symbol LIKE :searchParam ";
    return entityManager.createQuery(sqlQuery, Bar.class)
            .setParameter("searchParam", tickerNamePart)
            .getResultList()
            .stream()
            .map(bar -> bar.getInstId().getSymbol())
            .collect(Collectors.toList());
  }

  @Override
  public List<Bar> listByMarketAndTickerList(String market, String[] tickerList) {
    return entityManager.createQuery(SELECT_BY_MARKET_AND_TICKERS, Bar.class)
            .setParameter("market", "%@" + market)
            .setParameter("tickers", Arrays.asList(tickerList))
            .getResultList();
  }
}
