package com.gjj.igden.dao;

import com.gjj.igden.model.WatchListDesc;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

public interface WatchListDescDao {
  List<String> getAllStockSymbols(int id);

  List<WatchListDesc> getDataSetsAttachedToAcc(int id);

  WatchListDesc getWatchListDesc(int dsId, int accId);

  boolean addTicker(int watchlistId, String tickerName);

  boolean deleteWatchListDesc(int dsId, int accId);

  boolean deleteWatchListDesc(WatchListDesc watchListDesc);

  boolean createWatchListDesc(WatchListDesc watchListDesc);

  boolean updateWatchListDesc(WatchListDesc watchListDesc);
}
