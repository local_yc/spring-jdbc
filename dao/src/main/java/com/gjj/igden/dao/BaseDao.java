package com.gjj.igden.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class BaseDao <PK extends Serializable, T> {

  private final Class<T> persistentClass;

  @PersistenceContext
  protected EntityManager entityManager;

  @SuppressWarnings("unchecked")
  public BaseDao(){
    this.persistentClass =(Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
  }

	public T save(final T o) {
      return entityManager.merge(o);
    }

	public void delete(final Object object) {
      entityManager.remove(object);
    }

	public T get(PK id) {
      return entityManager.find(persistentClass, id);
    }

	public T merge(final T o) {
      return entityManager.merge(o);
    }

	public List<T> getAll() {
      return entityManager.createQuery("FROM " + persistentClass.getName(), persistentClass).getResultList();
    }
}
