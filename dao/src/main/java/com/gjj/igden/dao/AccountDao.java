package com.gjj.igden.dao;

import com.gjj.igden.model.Account;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.io.InputStream;
import java.util.List;

public interface AccountDao {

  List<Account> getAllAccounts();

  boolean delete(Account account);

  boolean delete(int id);

  Account get(Integer id);

  boolean setImage(int accId, InputStream is);

  byte[] getImage(int accId);

  boolean update(Account account);

  boolean create(Account account);
}
