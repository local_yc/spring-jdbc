package com.gjj.igden.dao.daoimpl;

import com.gjj.igden.dao.AccountDao;
import com.gjj.igden.dao.BaseDao;
import com.gjj.igden.model.Account;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Repository
public class AccountDaoImpl extends BaseDao<Integer, Account> implements AccountDao {

  public List<Account> getAllAccounts() {
    return entityManager.createQuery("SELECT a FROM Account a", Account.class).getResultList();
  }

  @Transactional
  public boolean delete(Account account) {
    Account entity = get(account.getId());
    if (entity != null) {
      super.delete(account);
      return true;
    }
    return false;
  }

  @Transactional
  public boolean delete(int id) {
    Account account = get(id);
    if (account != null) {
      delete(account);
      return true;
    }
    return false;
  }

  @Transactional
  public boolean setImage(int accId, InputStream is) {
    Account account = get(accId);
    try {
      if (account != null) {
        account.setImage(IOUtils.toByteArray(is));
        save(account);
        return true;
      }
    } catch (IOException e) {
      return false;
    }
    return false;
  }

  @Override
  public byte[] getImage(int accId) {
    return get(accId).getImage();
  }

  @Override
  public boolean update(Account account) {
    return merge(account) != null;
  }

  @Override
  public boolean create(Account account) {
    return save(account) != null;
  }
}
