package com.gjj.igden.dao.daoimpl;

import com.gjj.igden.dao.BaseDao;
import com.gjj.igden.dao.WatchListDescDao;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.WatchListDesc;
import com.gjj.igden.model.WatchListTicker;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class WatchListDescDaoImpl extends BaseDao<Integer, WatchListDesc> implements WatchListDescDao {

    public List<String> getAllStockSymbols(int watchListDescId) {
        return get(watchListDescId).getWatchListTickerList().stream()
                .map(WatchListTicker::getInstId)
                .collect(Collectors.toList());
    }

	public List<WatchListDesc> getDataSetsAttachedToAcc(int accId) {
		String query = "SELECT wl FROM WatchListDesc wl INNER JOIN wl.accountList a WHERE a.id = :account_id";
		return entityManager.createQuery(query, WatchListDesc.class)
				.setParameter("account_id", accId)
				.getResultList();
	}

    public WatchListDesc getWatchListDesc(int dsId, int accId) {
        String query = "SELECT wl FROM WatchListDesc wl INNER JOIN wl.accountList a " +
                "WHERE a.id = :account_id AND wl.datasetId = :data_set_id";
        return entityManager.createQuery(query, WatchListDesc.class)
                .setParameter("account_id", accId)
                .setParameter("data_set_id", dsId)
                .getSingleResult();
    }

	@Override
	public boolean addTicker(int watchlistId, String tickerName) {
        WatchListDesc watchListDesc = get(watchlistId);
        if (watchListDesc != null) {
            WatchListTicker watchListTicker = new WatchListTicker();
            watchListTicker.setInstId(tickerName);
            watchListTicker.setWatchListId(watchlistId);
            watchListDesc.getWatchListTickerList().add(watchListTicker);
            return true;
        }
        return false;
	}

	@Transactional
	public boolean deleteWatchListDesc(int dsId, int accId) {
        return deleteWatchListDesc(getWatchListDesc(dsId, accId));
    }

	@Transactional
	public boolean deleteWatchListDesc(WatchListDesc watchListDesc) {
		WatchListDesc entity = get(watchListDesc.getDatasetId());
		if (entity != null) {
		    entity.getAccountList().clear();
            super.delete(entity);
			return true;
		}
		return false;
	}

	@Transactional
	public boolean createWatchListDesc(WatchListDesc watchListDesc) {
		return save(watchListDesc) != null;
	}

	@Transactional
	public boolean updateWatchListDesc(WatchListDesc watchListDesc) {
		WatchListDesc entity = get(watchListDesc.getDatasetId());
		if (entity != null) {
			entity.setWatchListName(watchListDesc.getWatchListName());
            entityManager.merge(entity);
			return true;
		}
		return false;
	}
}
